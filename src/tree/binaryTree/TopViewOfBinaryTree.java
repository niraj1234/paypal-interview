package tree.binaryTree;

import tree.Tree;

import java.util.*;

class TopViewNode {
    Tree root;
    int index;

    TopViewNode(Tree root, int index) {
        this.root = root;
        this.index = index;
    }
}

public class TopViewOfBinaryTree {

    static List<Integer> findTopViewOfTreeWithTreeMap(Tree root) {
        if (root == null || (root.left == null && root.right == null))
            return Arrays.asList(root.value);

        Queue<TopViewNode> queue = new LinkedList<>();
        Map<Integer, Integer> map = new TreeMap<>();
        queue.add(new TopViewNode(root, 0));

        while (!queue.isEmpty()) {
            TopViewNode node = queue.poll();

            if (node.root.left != null) {
                queue.add(new TopViewNode(node.root.left, node.index - 1));
            }
            if (node.root.right != null) {
                queue.add(new TopViewNode(node.root.right, node.index + 1));
            }

            if (!map.containsKey(node.index)) {
                map.put(node.index, node.root.value);
            }
        }

        List<Integer> result = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : map.entrySet())
            result.add(entry.getValue());
        return result;
    }

    static List<Integer> findTopViewOfTreeBestApproach(Tree root) {
        if (root == null || (root.left == null && root.right == null))
            return Arrays.asList(root.value);

        Queue<TopViewNode> queue = new LinkedList<>();
        HashMap<Integer, Integer> map = new HashMap<>();
        queue.add(new TopViewNode(root, 0));

        int minIndex = 0;
        int maxValue = 0;
        while (!queue.isEmpty()) {
            TopViewNode node = queue.poll();

            if (node.root.left != null) {
                queue.add(new TopViewNode(node.root.left, node.index - 1));
                minIndex = Math.min(minIndex, node.index - 1);
            }
            if (node.root.right != null) {
                queue.add(new TopViewNode(node.root.right, node.index + 1));
                maxValue = Math.max(maxValue, node.index + 1);
            }

            if (!map.containsKey(node.index)) {
                map.put(node.index, node.root.value);
            }
        }

        List<Integer> result = new ArrayList<>();
        for (int i = minIndex; i <= maxValue; i++)
            result.add(map.get(i));
        return result;
    }

    public static void main(String[] args) {
        Tree tree = new Tree(1);
        tree.left = new Tree(2);
        tree.left.left = new Tree(4);
        tree.left.right = new Tree(5);
        tree.left.right.left = new Tree(6);

        tree.right = new Tree(3);
        tree.right.right = new Tree(7);

        System.out.println(findTopViewOfTreeBestApproach(tree));
        System.out.println(findTopViewOfTreeWithTreeMap(tree));
    }
}
