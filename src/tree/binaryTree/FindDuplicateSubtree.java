package tree.binaryTree;

import tree.Tree;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class FindDuplicateSubtree {
    static int findDuplicateSubtree(Tree root) {
        Map<String,Integer> map = new HashMap<>();
        duplicateSubtreeHelper(root, map);
        int counter = 0;
        for(Map.Entry<String,Integer> entry : map.entrySet()){
            if(entry.getValue() >= 2)counter++;
        }
        return counter;
    }

    static String duplicateSubtreeHelper(Tree root, Map<String, Integer> set) {
        if (root == null) return "$";
        String answer = "" + root.value;
        if(root.left == null && root.right == null){
            return answer;
        }
        String lp = duplicateSubtreeHelper(root.left, set);
        String rp = duplicateSubtreeHelper(root.right, set);
        answer = answer + lp + rp;
        if (set.containsKey(answer)) set.put(answer, set.get(answer) + 1);
        else set.put(answer, 1);
        return answer;
    }

    public static void main(String[] args) {
        Tree root = new Tree(1);
        root.left = new Tree(2);
        root.left.left = new Tree(4);
        root.left.right = new Tree(5);

        root.right = new Tree(3);
        root.right.right = new Tree(2);
        root.right.right.left = new Tree(4);
        root.right.right.right = new Tree(5);

        System.out.println(findDuplicateSubtree(root));

        root = new Tree(9);
        root.right = new Tree(6);
        System.out.println(findDuplicateSubtree(root));
    }
}
