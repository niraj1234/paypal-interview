package tree.binaryTree;

import tree.Tree;

/*
* If left tree height and right tree hight (lt - rt <= 1) then
* we can say its balanced binary tree.
* */
public class CheckTreeIsBalancedBinaryTree {

    boolean isBalancedBinaryTree(Tree root){
        return checkBalancedBinaryTree(root) != -1;
    }

    int checkBalancedBinaryTree(Tree root){
        if(root == null) return 0;
        int lh = checkBalancedBinaryTree(root.left);
        if(lh == -1) return -1;
        int rh = checkBalancedBinaryTree(root.right);
        if(rh == -1) return -1;

        if(Math.abs(lh - rh) > 1) return -1;

        return Math.max(lh, rh) + 1;
    }
}
