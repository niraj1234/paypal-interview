package tree.binaryTree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

class Node
{
    int data;
    Node left, right;

    Node(int item)
    {
        data = item;
        left = right = null;
    }
}


/*
    Level order traversal
    Easy Accuracy: 49.61% Submissions: 100k+ Points: 2
    Given a binary tree, find its level order traversal.
    Level order traversal of a tree is breadth-first traversal for the tree.

    Example 1:

    Input:
    1
    /   \
    3     2
    Output:1 3 2
    Example 2:

    Input:
    10
    /      \
    20       30
    /   \
    40   60
    Output:10 20 30 40 60
*/
public class LevelOrderTraversal {

    //Function to return the level order traversal of a tree.
    static ArrayList<Integer> levelOrder(Node node)
    {
        ArrayList<Integer> result = new ArrayList<>();
        Queue<Node> queue = new LinkedList<>();
        queue.add(node);

        while(!queue.isEmpty()){
            Node cNode = queue.poll();
            result.add(cNode.data);
            if(cNode.left != null)
                queue.add(cNode.left);
            if(cNode.right != null)
                queue.add(cNode.right);
        }
        return result;
    }
}
