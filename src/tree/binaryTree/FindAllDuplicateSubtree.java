package tree.binaryTree;

import tree.Tree;

import java.util.*;

public class FindAllDuplicateSubtree {

    static HashMap<String,Integer> map=new HashMap<>();
    static ArrayList<Tree> arr=new ArrayList<>();

   static String containdub(Tree node){
        if(node==null)return "$";
        String str ="";

        str=str+String.valueOf(node.value)+"*";
        str=str+containdub(node.left)+"*";
        str=str+containdub(node.right);

        if(map.containsKey(str)==true) {
            int of=map.get(str);
            int nf=of+1;
            map.put(str, nf);

        }else {
            map.put(str, 1);
        }

        if(map.get(str)==2) arr.add(node);

        return str;
    }


    public static List<Tree> printAllDups(Tree root)
    {
        containdub(root);
        return arr;
    }

    public static void main(String[] args) {
        Tree root = new Tree(1);
        root.left = new Tree(2);
        root.left.left = new Tree(4);
        root.left.right = new Tree(5);

        root.right = new Tree(3);
        root.right.right = new Tree(2);
        root.right.right.left = new Tree(4);
        root.right.right.right = new Tree(5);

        System.out.println(printAllDups(root));
    }
}
