package dfs;

/*
* 695. Max Area of Island
* https://leetcode.com/problems/max-area-of-island/
 * */
public class MaxAreaOfIsland {

    public int maxAreaOfIsland(int[][] grid) {
        int row = grid.length;
        int col = grid[0].length;
        if(row == 0 || col == 0){
            return 0;
        }
        int max = Integer.MIN_VALUE;

        for(int i = 0; i < row; i++){
            for(int j = 0; j < col; j++){
                if(grid[i][j] == 1){
                    max = Math.max(dfs(grid, i, j), max);
                }
            }
        }
        return max == Integer.MIN_VALUE ? 0 : max;
    }


    private static int dfs(int[][] grid, int i, int j){
        if(i<0 || j<0 || i>=grid.length || j>= grid[i].length || grid[i][j] == 0 || grid[i][j] == 2){
            return 0;
        }

        grid[i][j] = 2;

        return dfs(grid, i, j-1)+ dfs(grid, i, j+1)+dfs(grid, i-1, j)+dfs(grid, i+1, j) + 1;
    }
}
