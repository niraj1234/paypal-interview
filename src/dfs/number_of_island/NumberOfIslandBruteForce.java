package dfs.number_of_island;

public class NumberOfIslandBruteForce {

    //space : O(row*col)
    //Tine : O(row*col)
    public static int numberOfIsland(int[][] grid){
        int count = 0;
        boolean[][] cache = new boolean[grid.length][grid[0].length];
        for (int i=0; i<grid.length; i++){
            for (int j=0; j<grid[0].length; j++){
                if(!cache[i][j]){
                    gridHelper(grid, i, j, cache);
                    count++;
                }
            }
        }
        return count;
    }

    private static void gridHelper(int[][] grid, int i, int j, boolean[][] cache){
        if(i < 0 || j < 0 || i>=grid.length || j>=grid[0].length
        || cache[i][j]){
            return;
        }
        cache[i][j] = true;

        //Horizontal
        gridHelper(grid, i, j-1, cache);
        gridHelper(grid, i, j+1, cache);

        //vertical
        gridHelper(grid, i-1, j, cache);
        gridHelper(grid, i+1, j, cache);

        //diagonal
        gridHelper(grid, i-1, j-1, cache);
        gridHelper(grid, i+1, j+1, cache);
        gridHelper(grid, i-1, j+1, cache);
        gridHelper(grid, i+1, j-1, cache);
    }

    public static void main(String[] args) {
        int[][] grid = new int[5][5];
        grid[0][0] = 1;
        grid[0][1] = 0;
        grid[0][2] = 1;
        grid[0][3] = 0;
        grid[0][4] = 1;

        grid[1][0] = 1;
        grid[1][1] = 1;
        grid[1][2] = 0;
        grid[1][3] = 0;
        grid[1][4] = 0;

        grid[2][0] = 0;
        grid[2][1] = 0;
        grid[2][2] = 0;
        grid[2][3] = 0;
        grid[2][4] = 1;

        grid[3][0] = 0;
        grid[3][1] = 0;
        grid[3][2] = 1;
        grid[3][3] = 0;
        grid[3][4] = 0;

        grid[4][0] = 0;
        grid[4][1] = 1;
        grid[4][2] = 1;
        grid[4][3] = 0;
        grid[4][4] = 1;

        numberOfIsland(grid);
    }
}
