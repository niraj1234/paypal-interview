package linkedList;

public class ReverseLinkListForKGroup {

    public static Node reverse(Node head, int k)
    {
        Node prev = null;
        Node curr = head;
        Node next = null;
        int c = 0;

        while(curr != null && c < k){
            next = curr.next;
            curr.next = prev;
            prev = curr;
            curr = next;
            c++;
        }

        if(next != null) head.next = reverse(next, k);
        return prev;
    }

    public static void main(String[] args) {
        Node n = new Node(1);
        n.next = new Node(2);
        n.next.next = new Node(2);
        n.next.next.next = new Node(4);
        n.next.next.next.next = new Node(5);
        n.next.next.next.next.next = new Node(6);
        n.next.next.next.next.next.next = new Node(7);
        n.next.next.next.next.next.next.next = new Node(8);

        reverse(n, 4);
    }
}
