package array;

public class MedianOfTwoSortedArray {

    public static int medianOftwoSortedArrayBruteForce(int[] a, int[] b){
        if(a.length == 0)return findMid(b);
        else if(b.length == 0)return findMid(a);
        else{
            if(a.length > b.length){
                int[] t = a;
                a = b;
                b = t;
            }
            int l = 0;
            int r = a.length - 1;
            int total = a.length + b.length;
            int mid = total / 2;
            while(true){
                int m = l + (r - l) / 2;
                int p = mid - (m + 1) - 1;
                if(a[m] > b[p+1]) r = m - 1;
                else if(b[p] > a[m + 1]) l = m + 1;
                else{
                    if(total % 2 == 0){
                        return (Math.max(a[m], b[p]) + Math.min(a[m+1], b[p+1])) / 2;
                    }else{
                        return Math.min(a[m+1], b[p+1]);
                    }
                }
            }
        }
    }

    static void swap(int[] a, int[] b){
        if(a.length > b.length) {
            int[] temp = a;
            a = b;
            b = temp;
        }
    }

    static int findMid(int[] b){
        if(b.length % 2 == 0){
            int mid = b.length / 2;
            return (b[mid] + b[mid - 1]) / 2;
        }else return b[b.length / 2];
    }

    public static void main(String[] args) {
        /*System.out.println(
                medianOftwoSortedArrayBruteForce(
                        new int[]{1,2,4,5,8,9},
                        new int[]{2,6,6,7,8,9})
        );*/

        System.out.println(
                medianOftwoSortedArrayBruteForce(
                        new int[]{1,3},
                        new int[]{2})
        );

        System.out.println(
                medianOftwoSortedArrayBruteForce(
                        new int[]{1,2},
                        new int[]{3,4})
        );
    }
}
