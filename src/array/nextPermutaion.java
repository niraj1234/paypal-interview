package array;

import java.util.Arrays;

/*
* Time : O(n)
* Space : O(1)
* https://leetcode.com/problems/next-permutation/
* 31. Next Permutation
* Medium
*
    Example 1:
    Input: nums = [1,2,3]
    Output: [1,3,2]

    Example 2:

    Input: nums = [3,2,1]
    Output: [1,2,3]
    Example 3:

    Input: nums = [1,1,5]
    Output: [1,5,1]
* */
public class nextPermutaion {

    public void nextPermutation(int[] nums) {
        int n = nums.length;
        if(n == 1) return;

        int idx = -1;

        //Find the element which is greter then its previous number.
        for(int i = n - 1; i > 0; i--){
            if(nums[i] > nums[i-1]){
                idx = i;
                break;
            }
        }
        // if all are in decending order return reverse means asending order.
        if(idx == -1) reverse(nums, 0, n -1);
        else{
            int prev = idx;
            for(int i = idx + 1; i < n; i++){
                if(nums[i] > nums[idx - 1] && nums[i] <= nums[prev]){
                    prev = i;
                }
            }

            swap(nums, idx - 1, prev);
            reverse(nums, idx, n-1);
        }
    }

    void nextPermutationApproach2(int[] arr){
        if(arr == null || arr.length <= 1) return;
        int i = 0;
        while(i < arr.length-1 && arr[i] < arr[i+1]) i++;

        if(i == 0){
            reverse(arr, 0, arr.length - 1);
            return;
        }
        i--;
        int j = arr.length - 1;
        while(arr[j] < arr[i]) j--;
        swap(arr, i, j);
        reverse(arr, i + 1, arr.length-1);
    }


    void nextPermutationApproach3(int[] arr){

        /*
        * Intuation step
        *
        * 1,3,5,4,2
        * Step 1: Traverse from back until a[i] < a[i+1]
        *         e.g. 3 < 5 so here my last i will be idx 1 = 3
        * Step 2: Traverse from back until a[j] <= a[i]
        *         e.g 4 > 3 so here my last j will be idx 3 = 4
        * Step 3: swap i with j so, new array will be a
        *         1, 4, 5, 3, 2
        * Step 4: Reverse array i + 1 to n - 1 so, new array will be a
        *         1, 4, 2, 3, 5
        * */
        if(arr == null || arr.length <= 1) return;
        int i = arr.length - 2;
        while(i >= 0 && arr[i] >= arr[i+1]) i--;

        if(i >= 0){
            int j = arr.length - 1;
            while(arr[j] <= arr[i]) j--;
            swap(arr, i, j);
        }
        reverse(arr, i+1, arr.length - 1);
    }

    private void reverse(int[] nums, int start, int end){
        while(start < end){
            swap(nums, start, end);
            start++;
            end--;
        }
    }

    private void swap(int[] nums, int start, int end){
        int temp = nums[start];
        nums[start] = nums[end];
        nums[end] = temp;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{1,3,5,4,2};
        new nextPermutaion().nextPermutationApproach3(arr);
        for (int value:arr) System.out.print(value);
        System.out.println();

        arr = new int[]{5,4,3,2,1};
        new nextPermutaion().nextPermutationApproach3(arr);
        for (int value:arr) System.out.print(value);
        System.out.println();

        arr = new int[]{1, 2, 3, 6, 5, 4};
        new nextPermutaion().nextPermutationApproach3(arr);
        for (int value:arr) System.out.print(value);
        System.out.println();


        arr = new int[]{3, 2, 1};
        new nextPermutaion().nextPermutationApproach3(arr);
        for (int value:arr) System.out.print(value);
        System.out.println();

        arr = new int[]{1,2,5,3,4};
        new nextPermutaion().nextPermutationApproach3(arr);
        for (int value:arr) System.out.print(value);
        System.out.println();
    }
}
