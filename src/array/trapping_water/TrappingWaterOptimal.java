package array.trapping_water;

import java.awt.print.Pageable;

public class TrappingWaterOptimal {

    static int trap(int arr[]){
        int n = arr.length;
        int l = 0, r = n - 1;

        int res = 0;

        int maxL = 0, maxR = 0;

        while (l <= r){
            int currentL = arr[l];
            int currentR = arr[r];
            if(currentL <= currentR) {
                if (currentL >= maxL) maxL = currentL;
                else res += maxL - arr[l];
                l++;
            }else {
                if (currentR >= maxR) maxR = currentR;
                else res += maxR - currentR;
                r--;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println(trap(new int[]{0,1,0,2,1,0,1,3,2,1,2,1}));
        System.out.println(trap(new int[]{4,2,0,3,2,5}));
    }
}
