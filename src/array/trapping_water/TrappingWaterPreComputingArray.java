package array.trapping_water;

/*
* https://leetcode.com/problems/trapping-rain-water/
* 42. Trapping Rain Water
  Hard

  Time: O(3N) where 1st N is for computing prefix array
  * 2nd for computing suffix array and last for calculate answer.

  Space: O(2N) for suffix and prefix.
* */
public class TrappingWaterPreComputingArray {

    static int trapWater(int arr[]){
        int sum = 0;

        int[] prefix = new int[arr.length];
        int[] suffix = new int[arr.length];
        int max = Integer.MIN_VALUE;
        for(int i = 0; i < arr.length; i++){
            prefix[i] = max = Math.max(max, arr[i]);
        }

        max = Integer.MIN_VALUE;
        for(int i = arr.length - 1; i >= 0; i--){
            suffix[i] = max = Math.max(max, arr[i]);
        }

        for(int i = 1; i < arr.length-1; i++){
            sum += Math.min(prefix[i], suffix[i]) - arr[i];
        }
        return sum;
    }

    public static void main(String[] args) {
        System.out.println(trapWater(new int[]{0,1,0,2,1,0,1,3,2,1,2,1}));
        System.out.println(trapWater(new int[]{4,2,0,3,2,5}));
    }
}
