package array.trapping_water;

/*
* https://leetcode.com/problems/trapping-rain-water/
* 42. Trapping Rain Water
  Hard

  Time: O(N^2)
* */
public class TrappingWaterBruteForce {
    static int trapWater(int arr[]){
        int sum = 0;
        for(int i = 1; i < arr.length-1; i++){
            sum += Math.min(left(arr, i), right(arr, i)) - arr[i];
        }
        return sum;
    }

    static int left(int arr[], int index){
        int max = Integer.MIN_VALUE;
        for(int i = index; i >= 0; i--){
           max = Math.max(max, arr[i]);
        }
        return max;
    }

    static int right(int arr[], int index){
        int max = Integer.MIN_VALUE;
        for(int i = index; i < arr.length; i++){
            max = Math.max(max, arr[i]);
        }
        return max;
    }

    public static void main(String[] args) {
        //System.out.println(trapWater(new int[]{0,1,0,2,1,0,1,3,2,1,2,1}));
        System.out.println(trapWater(new int[]{4,2,0,3,2,5}));
    }
}
