package array;

public class Sort012 {
    public static void sort012(int a[], int n)
    {
        int arr[] = new int[3];
        for(int i = 0; i < a.length; i++){
            arr[a[i]]++;
        }

        int k = 0;
        for(int i = 0; i < a.length; i++){
            if(arr[k] == 0) k++;
            a[i] = k;
            arr[k]--;
        }
    }

    public static void main(String[] args) {
        sort012(new int[]{0,2,1,2,0}, 5);
    }
}
