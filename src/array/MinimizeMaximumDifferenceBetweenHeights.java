package array;

import java.util.Arrays;

public class MinimizeMaximumDifferenceBetweenHeights {

    static int minDiff(int[] arr, int k){
        Arrays.sort(arr);
        int n = arr.length;
        int ans  =  arr[n-1] - arr[0];

        int smallest = arr[0] + k;
        int largest = arr[n-1] - k;

        int min, max;

        for(int i = 0; i < n-1; i++){
            min = Math.min(smallest, arr[i+1]-k);
            max = Math.max(largest, arr[i]+k);

            if(min < 0) continue;

            ans = Math.min(ans, max - min);
        }
        return ans;
    }

    public static void main(String[] args) {
        minDiff(new int[]{3,6,9,16,12,20}, 3);
        System.out.println(minDiff(new int[]{2,6,3,4,7,2,10,3,2,1}, 5));
    }
}
