package dp.Knapsack_0_1;

public class Knapsack_01_Memoization {

    public static void main(String[] args) {
        int[] weigth = new int[]{1, 3, 4, 5};
        int[] value = new int[]{1, 4, 5, 7};
        int w = 11;
        int n = 4;

        int[][] dp = new int[n + 1][w + 1];
        for (int i = 0; i < n + 1; i++) {
            for (int j = 0; j < w + 1; j++) {
                dp[i][j] = -1;
            }
        }
        System.out.println(knapsack(weigth, value, 4, w, dp));
    }

    private static int knapsack(int[] weigth, int[] value, int n, int w, int[][] dp) {

        // Base Condition
        if (n == 0 || w == 0) return 0;

        // if matrix has contains positive value then return its value
        if (dp[n][w] != -1) return dp[n][w];

        /* Current weight not exceed total weight
         * then we take max(include current item or exclude current item)
         * */
        if (weigth[n - 1] <= w) {
            return dp[n][w] = Math.max(
                    value[n - 1] + knapsack(weigth, value, n - 1, w - weigth[n - 1], dp), //include current item(value[n-1]) + recursive call for find other
                    knapsack(weigth, value, n - 1, w, dp) // exclude current item
            );
        } else { // if item weight is > total weight then exclude that item and go for next sub problem
            return dp[n][w] = knapsack(weigth, value, n - 1, w, dp);
        }
    }
}
