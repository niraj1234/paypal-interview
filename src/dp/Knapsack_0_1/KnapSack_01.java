package dp.Knapsack_0_1;

public class KnapSack_01 {

    public static void main(String[] args) {
        int[] weigth = new int[]{1, 3, 4, 5};
        int[] value = new int[]{1, 4, 5, 7};
        int w = 11;

        System.out.println(knapsack(weigth, value, 4, w));
    }

    private static int knapsack(int[] weigth, int[] value, int n, int w) {
        // Base condition
        if(n == 0 || w == 0){
            return 0;
        }

        /* Current weight not exceed total weight
        * then we take max(include current item or exclude current item)
        * */
        if(weigth[n - 1] <= w){
            return Math.max(
                    value[n-1] + knapsack(weigth, value, n-1, w - weigth[n-1]), //include current item(value[n-1]) + recursive call for find other
                    knapsack(weigth, value, n-1, w) // exclude current item
            );
        } else{ // if item weight is > total weight then exclude that item and go for next sub problem
            return knapsack(weigth, value, n-1, w);
        }
    }
}
