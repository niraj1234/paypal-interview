package dp.Knapsack_0_1;

public class Knapsack_01_DP {

    public static void main(String[] args) {
        int[] weigth = new int[]{1, 3, 4, 5};
        int[] value = new int[]{1, 4, 5, 7};
        int w = 8;
        int n = 4;

        int[][] dp = new int[n + 1][w + 1];

        for(int i = 0; i < n + 1; i++){
            for(int j = 0; j< w + 1; j++){

                if(i == 0 || j == 0) {
                    dp[i][j] = 0;
                    continue;
                }

                if(weigth[i - 1] <= j)
                    dp[i][j] = Math.max(
                            value[i-1] + dp[i-1][j - weigth[i-1]],
                            dp[i-1][j]);
                else dp[i][j] = dp[i-1][j];
            }
        }
        System.out.println(dp[n][w]);
    }
}
