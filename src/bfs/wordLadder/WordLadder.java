package bfs.wordLadder;

import java.util.*;

/*
* https://leetcode.com/problems/word-ladder/
*
        127. Word Ladder  -  Hard

        A transformation sequence from word beginWord to word endWord using a dictionary wordList is a sequence of words beginWord -> s1 -> s2 -> ... -> sk such that:

        Every adjacent pair of words differs by a single letter.
        Every si for 1 <= i <= k is in wordList. Note that beginWord does not need to be in wordList.
        sk == endWord
        Given two words, beginWord and endWord, and a dictionary wordList, return the number of words in the shortest transformation sequence from beginWord to endWord, or 0 if no such sequence exists.
* */
public class WordLadder {
    public static int ladderLength(String beginWord, String endWord, List<String> wordList) {
        //sanity check
        if (beginWord.equals(endWord) || !wordList.contains(endWord)) {
            return 0;
        }

        //build a map for quick lookup possible next word
        Map<String, List<String>> wordMap = new HashMap<>();
        for (String word : wordList) {
            char[] wordArr = word.toCharArray();
            for (int i = 0; i < word.length(); i++) {
                char temp = wordArr[i];
                wordArr[i] = '*';
                String key = new String(wordArr);
                if (!wordMap.containsKey(key)) {
                    wordMap.put(key, new ArrayList<>());
                }
                wordMap.get(key).add(word);
                wordArr[i] = temp;  //restore original char for next loop
            }
        }

        //BFS
        Set<String> visited = new HashSet<String>();
        Queue<String> queue = new LinkedList<String>();
        queue.add(beginWord);
        int count = 1;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                String word = queue.remove();
                visited.add(word);
                char[] wordArr = word.toCharArray();
                for (int j = 0; j < wordArr.length; j++) {
                    char temp = wordArr[j];
                    wordArr[j] = '*';
                    String key = new String(wordArr);
                    if (wordMap.containsKey(key)) {
                        List<String> words = wordMap.get(key);
                        for (String nextWord : words) {
                            if (!visited.contains(nextWord)) {
                                if (nextWord.equals(endWord)) {
                                    return count + 1;
                                }
                                queue.add(nextWord);
                            }
                        }
                    }
                    wordArr[j] = temp; //restore original char for next loop
                }
            }
            count++;
        }
        return 0;
    }

    public static void main(String[] args) {
        ladderLength("hit", "cog", Arrays.asList("hot","dot","dog","lot","log","cog"));
    }
}
