package algoritham.binary_search;

/*
https://leetcode.com/problems/search-in-rotated-sorted-array/
* 33. Search in Rotated Sorted Array
* Medium
* */
public class SearchInRotatedArray {
    public int search(int[] nums, int target) {
        int peak = findPeak(nums);
        if(peak == -1) return -1;
        int result = binarySearch(nums, target, 0, peak);
        if(result != -1) return result;
        return binarySearch(nums, target, peak + 1, nums.length-1);


        /*
        * Also, we can do if(peak == target) return peak
        * else if(target >= nums[start]) return bs(nums, target, 0, peak - 1);
        * return bs(nums, target, peak + 1, len);
        * */
    }

    private int findPeak(int[] nums){
        int len = nums.length;
        if(len == 0) return -1;
        if(nums[0] < nums[len-1]) return len-1;

        int start = 0;
        int end = len - 1;

        /*
        * E.G 4, 5, 6, 7, 3, 2, 1
        *     s        m        e
        * */
        while(start < end){
            int mid = start + (end - start) / 2;

            /*
            * Here first 2 check for find the pivot.
            * if my mid is not a last no to prevent ArrayIndexOutofBound end value of mid is > value of mid + 1
            * then we know that array is sorted hence mid is my pivot.
            *
            * if mid is 7 and if it pivot then mid + 1 is always smaller than it
            * */
            if(mid < end && nums[mid] > nums[mid + 1]){
                return mid;
            }
            /*
            * here same for like current element - 1 index is a pivot then
            * we check that nums[mid] < nums[mid - 1] and mid should be a
            * first element to prevent ArrayIndexOutofBound
            *
            * if mid is 3 then mid - 1 is a pivot
            * */
            else if(mid > start && nums[mid] < nums[mid - 1]){
                return mid -1;
            }
            /*
            * If mid = 2 then my answer would lie on mid - 1
            * */
            else if(nums[mid] < nums[start]){
                end = mid - 1;
            }
            /*
            * In first 2 cases we are handle the pivot cases hence if
            * nums[mid] > nums[start] then we can assign start = mid + 1
            * */
            else{
                start = mid + 1;
            }
        }
        return start;
    }

    private int findPeakWithDuplicate(int[] nums){
        int len = nums.length;
        if(len == 0) return -1;
        if(nums[0] < nums[len-1]) return len-1;

        int start = 0;
        int end = len - 1;

        while(start < end){
            int mid = start + (end - start) / 2;
            if(mid < end && nums[mid] > nums[mid + 1]){
                return mid;
            } else if(mid > start && nums[mid] < nums[mid - 1]){
                return mid -1;
            }
            /* e.g. 2 1 1 2 2 2 2 2
                    s     m       e
               e.g  1 1 1 1 1 1 2 1
                    s     m       e
            */
            else if(nums[start] == nums[mid] && nums[end] == nums[mid]){
                if(nums[start] > nums[start+1]) return start;
                start++;
                if(nums[end] < nums[end-1]) return end - 1;
                end--;
            }
            /*
                e.g  1 1 1 1 1 1 2 0
                     s     m       e
            * */
            else if(nums[start] < nums[mid] || (nums[start] == nums[mid] && nums[end] < nums [mid])){
                start = mid + 1;
            } else{
                end = mid - 1;
            }
        }
        return start;
    }

    private int binarySearch(int[] nums, int target, int start, int end){
        while(start <= end){
            int mid = start + (end - start) / 2;
            if(nums[mid] < target) start = mid + 1;
            else if (nums[mid] > target) end = mid - 1;
            else return mid;
        }
        return -1;
    }
}
