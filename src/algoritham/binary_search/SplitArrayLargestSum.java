package algoritham.binary_search;

/*
* https://leetcode.com/problems/split-array-largest-sum/
* 410. Split Array Largest Sum
* Hard - Asked by Google
* */
public class SplitArrayLargestSum {


    /*
    Input: nums = [7,2,5,10,8], m = 2
    Output: 18
    Explanation:
    There are four ways to split nums into two subarrays.
    The best way is to split it into [7,2,5] and [10,8],
    where the largest sum among the two subarrays is only 18.
    */

    /*
     *  How to calculate answer:
     *  Here m = 2, hence we have to make 2 partitions
     *  max(sum([7]),sum([2,5,10,8])) = 25
     *  max(sum([7,2]),sum([5,10,8])) = 23
     *  max(sum([7,2,5]),sum([10,8])) = 18
     *  max(sum([7,2,5,10]),sum([8])) = 24
     *
     *  Min of all max is 18 is our answer
     * */
    public int splitArray(int[] nums, int m) {

        int start = 0;
        int end  = 0;

        /*
            Here we are calculate our start and end pointer.
            if m = 1 which is minimum partition then max will be 32
            sum[7,2,5,10,8] = 32
            if m = n which is maximum partition then max will be 10
            Max([7],[2],[5],[10],[8]) = 10
        */
        for(int num : nums) {
            start = Math.max(start, num);
            end += num;
        }

        while(start < end){
            int mid = start + (end - start) / 2;

            int sum = 0;
            int pieces = 1;

            for(int num : nums){
                /* If current sum + current number exceeds my mid then 
                *  sum start with current num and we can say my second subarray
                *  will start so pieces++;
                *
                *  mid = 21
                *  e.g sum = [7, 2, 5] = 14, pieces = 1
                *  but sum + num = 22 so we can not put in same array
                *  hence we add new sub array like [8] and increase pieces.
                *  at the end partition look like [7, 2, 5], [8, 10]
                * */
                if(sum + num > mid){
                    sum = num;
                    pieces++;
                }else{
                    sum += num;
                }
            }

            /* If total partition exceed my limit(m) then we have to decrease
            *  the range with start = mid + 1;
            *  else if pieces <= m then we can say that mid can be a possible answer
            *  for m partitions hence we make end = mid;
            * */
            if(pieces > m) start = mid + 1;
            else end = mid;
        }

        return start;// here start end are same hence return any of them.
    }

}
