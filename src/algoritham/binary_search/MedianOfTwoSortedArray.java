package algoritham.binary_search;

/*
* Time : O(log(MIN(n1, n2))) because we run binary search on smaller array
* Space: O(1)
* */
public class MedianOfTwoSortedArray {

    static double findMedianOfTwoSortedArray(int[] nums1, int[] nums2) {
        // If num1 is greater than num2 the we just replace it by each other
        if (nums1.length > nums2.length) return findMedianOfTwoSortedArray(nums2, nums1);
        int n1 = nums1.length;
        int n2 = nums2.length;

        int low = 0;
        int high = n1; // here we targeting smaller array for binary search hence
                       // take nums1 length;

        while (low <= high) {
            int cut1 = (low + high) / 2; // mid of first array
            int cut2 = ((n1 + n2 + 1) / 2) - cut1; // mid of second array - no of element selected from first array

            /* if cut 1 or cut 2 is zero
            *  e.g
            *       cut 1
            *         0  1   2   3   4
            * nums1 : 7, 12, 14, 15, 16
            * nums2 : 1, 2, 3, 4, 9, 11
            *         0  1  2  3  4  5
            *                       cut 2
            * Here cut 1 is 0 hence we can not calculate l1 so we set as min number
            * Same for l2 if cut2 == 0.
            *  */
            int l1 = cut1 == 0 ? Integer.MIN_VALUE : nums1[cut1 - 1];
            int l2 = cut2 == 0 ? Integer.MIN_VALUE : nums2[cut2 - 1];

            int r1 = cut1 == n1 ? Integer.MAX_VALUE : nums1[cut1];
            int r2 = cut2 == n2 ? Integer.MAX_VALUE : nums2[cut2];

            /*         l1 r1
             * nums1 : 7, 12, 14, 15, 16
             * nums2 : 1, 2, 3, 4, 9, 11
             *                     l2 r2
             */
            if (l1 <= r2 && l2 <= r1) {
                if ((n1 + n2) % 2 == 0)
                    return (Math.max(l1, l2) + Math.min(r1, r2)) / 2; // calculation for even number
                else return Math.max(l1, l2); // calculation for odd number
            }
            /* Here we applying binary search on nums1 by change high and low by cut 1 which in nums 1*/
            else if(l1 > r2) high = cut1 - 1;
            else low = cut1 + 1;
        }
        return 0.0;
    }

    public static void main(String[] args) {
        System.out.println(findMedianOfTwoSortedArray(new int[]{7,12,14,15,16}, new int[]{1,2,3,4,9,11}));
        System.out.println(findMedianOfTwoSortedArray(new int[]{7,12,14,15,16}, new int[]{1,2,3,4,9,11,12}));
    }
}
