package algoritham.sliding_window;

public class MaxSubarrayAverage {
    public double findMaxAverage(int[] nums, int k) {
        double result = 0.0;
        double currentRes = 0.0;
        if(nums.length < k) return result;

        if(nums.length == 1) return nums[0] / k;
        int i = 0;
        int j = i + 1;
        double sum = nums[i] + nums[j];
        while(j < nums.length - 1){

            if((j + 1 - i) != k){
                j++;
                sum += nums[j];
            }
            else{
                currentRes = sum / k;
                sum -= nums[i];
                i++;
            }

            if(currentRes > result) result = currentRes;
        }

        return result;
    }

    public static void main(String[] args) {
        new MaxSubarrayAverage().findMaxAverage(new int[]{1,12,-5,-6,50,3}, 4);
        //new MaxSubarrayAverage().findMaxAverage(new int[]{5}, 1);
    }
}
