package algoritham.sliding_window;

import java.util.*;

public class MaximummFromAllSubArray {
    static List<Integer> findMAximumFromAllSubArray(List<Integer> list, int k){
        List<Integer> result = new ArrayList<>();
        Queue<Integer> queue = new LinkedList<>();
        if(k >= list.size()) result.add(list.stream().mapToInt(v -> v).max().orElse(-1));

        int i = 0;
        int j = 0;

        while(j < list.size()){
            while(queue.size() > 0 && queue.peek() < list.get(j)){
                queue.poll();
            }
            queue.add(list.get(j));
            if(j - i + 1 < k) j++;
            else if(j - i + 1 == k){
                int value = queue.peek();
                result.add(value);
                if(value == list.get(i)) queue.poll();
                i++;
                j++;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        List<Integer> mAximumFromAllSubArray = findMAximumFromAllSubArray(Arrays.asList(1, 3, -1, -3, 5, 3, 6, 7), 3);
        System.out.println(mAximumFromAllSubArray);
    }
}
