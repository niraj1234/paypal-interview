package algoritham.sliding_window;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FindAnagram {

    public List<Integer> findAnagrams(String s, String p) {
        int i = 0;
        int j = 0;

        List<Integer> result = new ArrayList<>();

        HashMap<Character, Integer> map =  new HashMap<>();

        for(char c : p.toCharArray()){
            if(map.containsKey(c)) map.put(c, map.get(c) + 1);
            else map.put(c, 1);
        }

        int counter = map.size();
        int k = p.length();

        while(j < s.length()){
            char c = s.charAt(j);
            if(j - i + 1 < k) {
                if(map.containsKey(c)){
                    map.put(c, map.get(c) - 1);
                    if(map.get(c) == 0) counter--;
                }
                //System.out.println(counter);
                j++;
            }else{
                if(map.containsKey(c)){
                    if(map.get(c) > 0) map.put(c, map.get(c) - 1);
                    else {
                        map.put(c, map.get(c) - 1);
                        counter++;
                    }
                    if(map.get(c) == 0) counter--;
                }
                if(counter == 0) result.add(i);

                if(map.containsKey(s.charAt(i))) {
                    if(map.get(s.charAt(i)) == 0) counter++;
                    map.put(s.charAt(i), map.get(s.charAt(i)) + 1);
                    if(map.get(s.charAt(i)) == 0) counter--;
                }
                i++;
                j++;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(new FindAnagram().findAnagrams("aa", "bb"));
    }
}
