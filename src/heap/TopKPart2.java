package heap;

import java.util.*;

class Node11 implements Comparable<Node>{
    int key;
    int value;

    Node11 (int key, int value){
        this.key = key;
        this.value = value;
    }

    public int compareTo(Node o){
        if(this.value > o.value) return -1;
        else if(this.value < o.value) return 1;
        else return 0;
    }
}

class TopKFrequentElement{
    public List<Integer> findTopKFrequentElement(List<Integer> list, int k){
        PriorityQueue<Node11> queue = new PriorityQueue<>();
        Map<Integer, Integer> map = getFrequencyMap(list);

        for(Map.Entry<Integer, Integer> entry : map.entrySet()){
            if(queue.size() < k){
                queue.add(new Node11(entry.getKey(), entry.getValue()));
            }else{
                Node11 node =  queue.peek();
                if(node.value < entry.getValue()){
                    queue.poll();
                    queue.add(new Node11(entry.getKey(), entry.getValue()));
                }
            }
        }
        List<Integer> result = new ArrayList<>();
        while(!queue.isEmpty()){
            Node11 node = queue.poll();
            result.add(node.key);
        }
        return result;
    }

    private static Map<Integer, Integer> getFrequencyMap(List<Integer> list){
        Map<Integer, Integer> map = new HashMap<>();
        for(int value: list){
            if(map.containsKey(value)){
                map.put(value, map.get(value) + 1);
            }else map.put(value, 1);
        }
        return map;
    }
}

