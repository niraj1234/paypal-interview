package heap;

import java.util.PriorityQueue;
import java.util.Queue;

public class SortKSortedArray {
    static void kSortedArrayByHeap(int[] arr, int k){
        Queue<Integer> queue = new PriorityQueue<>();
        int currentIdx = 0;
        for(int i = 0; i < arr.length; i++){
            if(queue.size() <= k){
                queue.add(arr[i]);
            }
            if(queue.size() > k){
                int poll = queue.poll();
                arr[currentIdx] = poll;
                currentIdx++;
            }
        }
        if(queue.size() > 0){
            for(int i = currentIdx; i < arr.length; i++){
                arr[i] = queue.poll();
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = new int[]{6,5,3,2,8,10,9};
        kSortedArrayByHeap(arr, 3);
        kSortedArrayByHeap(new int[]{1,8,9,2,0,6}, 4);
    }
}
