package heap;

import java.util.Collections;
import java.util.PriorityQueue;

public class MedianOfRunningStream {

    private PriorityQueue<Integer> maxHeap =  new PriorityQueue<>(Collections.reverseOrder());
    private PriorityQueue<Integer> minHeap =  new PriorityQueue<>();

    public void insertNum(int num){
        if (maxHeap.isEmpty() || maxHeap.peek() >= num)
            maxHeap.add(num);
        else
            minHeap.add(num);

        // either both the heaps will have equal number of elements or max-heap will have
        // one or more element than the min-heap
        if(maxHeap.size() > minHeap.size() + 1)
            minHeap.add(maxHeap.poll());
        else if(maxHeap.size() < minHeap.size())
            maxHeap.add(minHeap.poll());
    }

    public double findMedian(){
        if(maxHeap.size() == minHeap.size())
            return (maxHeap.peek() + minHeap.peek()) / 2;
        return maxHeap.peek();
    }

    public static void main(String[] args) {
        MedianOfRunningStream m = new MedianOfRunningStream();
        m.insertNum(5);
        m.insertNum(3);
        System.out.println(m.findMedian());
        m.insertNum(4);
        m.insertNum(2);
        System.out.println(m.findMedian());
        m.insertNum(6);
        System.out.println(m.findMedian());
    }
}
